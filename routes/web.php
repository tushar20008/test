<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/fuzz/{number}', function ($number) use ($router) {
	$output[1] = null;	
	for ($i = 1; $i <= $number; $i++) {
    		if($i % 3 == 0 && $i % 5 == 0){
    			$output[$i] = "FizzBuzz";
    		}
    		else if($i % 3 == 0){
    			$output[$i] = "Fizz";
    		}
    		else if($i % 5 == 0){
    			$output[$i] = "Buzz";
    		}
    		else{
    			$output[$i] = $i;
    		}
	} 
	return response()->json($output);
});

$router->get('test', function () use ($router) {
    return 'Welcome to The Trip Guru Challenge';
});
$router->get('/', function () use ($router) {
    return $router->app->version();
});
