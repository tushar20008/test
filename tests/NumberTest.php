<?php

class NumberTest extends TestCase
{
    /**
     * @Test
     * @return void
     */
    public function testExample()
    {
	$postRequests = array(
		"/fuzz/1",
		"/fuzz/5",
		"/fuzz/10",
		"/fuzz/15"
		);   
	
	$expectedJson[0][1] = 1;
	for ($i = 5; $i <= 15;) {
		for($j = 1; $j<= $i; $j++){
    			if($j % 3 == 0 && $j % 5 == 0){
    				$expectedJson[$i/5][$j] = "FizzBuzz";
    			}
    			else if($j % 3 == 0){
    				$expectedJson[$i/5][$j] = "Fizz";
    			}
    			else if($j % 5 == 0){
    				$expectedJson[$i/5][$j] = "Buzz";
    			}
    			else{
    				$expectedJson[$i/5][$j] = $j;
    			}
		} 
		$i += 5;
     	}
	
	for($i=0; $i<count($postRequests); $i++){
		$this->get($postRequests[$i])->seeJsonEquals($expectedJson[$i]);
	}
    }
}

